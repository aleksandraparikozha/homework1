/*დაწერეთ ფუნქცია, რომელსაც გადაეცემა String ტიპის პარამეტრი და დააბრუნებს Boolean
მნიშვნელობას.ფუნქციამ უნდა დააბრუნოს true, თუ გადმოცემული String მნიშვნელობა
პალინდრომია, წინააღმდეგ შემთხვევაში false.
(პალინდრომი— სიტყვა, ფრაზა ან ლექსი, რომელიც წაღმა და უკუღმა ერთნაირად იკითხება)
(2.5ქ)*/


fun main(){

    println(palindrome("ababa"))
    println(palindrome("lecture"))
}


fun palindrome(word: String): Boolean{
    var wordPal = ""

    for (i in word.length-1 downTo 0){
        wordPal+=word[i]
    }

    return word==wordPal


}