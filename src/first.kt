/*დაწერეთ ფუნქცია, რომელიც პარამეტრად მიიღებს მთელი რიცხვების მასივს და დააბრუნებს
მთელ მნიშვნელობას.ფუნქციამ უნდა გამოთვალოს მასივში ლუწ ინდექსზე მდგომი რიცხვების
საშუალო არითმეტიკული.
მოიყვანეთ ფუნქციის გამოყენების მაგალითი.
(2.5ქ)
*/


fun main(){
    val newArray = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8)
    println(Average(newArray))
}

fun Average(numArray: IntArray): Int{
    val length = numArray.size
    var sum = 0

    for (i in 0 until length){
        if(i%2==0){
            sum += numArray[i]
        }

    }

    return sum

}